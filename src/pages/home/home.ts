import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public peso: string;
  public altura: string;
  public result: number;
  public text: string;

  constructor(public navCtrl: NavController) {

  }

  calculoimc(){
    let pes = parseFloat(this.peso);
    let alt = parseFloat(this.altura);
    
    this.result = pes / (2*alt);

    if(this.result < 18.5){
      this.text = "Abaixo do Peso!";
    }
    else if(this.result >= 18.5 && this.result < 24.9){
      this.text = "Peso Normal!";
    }
    else if(this.result >= 24.9 && this.result <= 29.9){
      this.text = "Sobrepeso!!";
    }
    else if(this.result >= 30 && this.result <= 34.9){
      this.text = "Obesidade Grau I!!";
    }
    else if(this.result >= 35 && this.result <= 39.9){
      this.text = "Obesidade Grau II!!!";
    }
    else if(this.result >= 40){
      this.text = "Obesidade Grau III ou Mórbida.";
    }
  }
}
